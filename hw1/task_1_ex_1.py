"""01-Task1-Task1
Write a Python-script that performs simple arithmetic operations: '+', '-', '*', '/'. The type of operator and
data are set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py 1 * 2

Notes:
For other operations need to raise NotImplementedError.
Do not dynamically execute your code (for example, using exec()).
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""
import argparse


parser = argparse.ArgumentParser(description='Process expression to evaluate.')
parser.add_argument('expr', metavar='expression', type=str, nargs='+',
                    help='string expression')


def calculate(args):
    operations = {'+': lambda x, y: x + y, '-': lambda x, y: x - y,
                  '*': lambda x, y: x * y, '/': lambda x, y: x / y}
    operation = args[1]
    if operation in operations.keys():
        return operations[operation](float(args[0]), float(args[2]))
    raise NotImplementedError(f'{operation} is not implemented!')


def main():
    args = parser.parse_args().expr
    print(calculate(args))


if __name__ == '__main__':
    main()

"""01-Task1-Task2
Write a Python-script that performs the standard math functions on the data. The name of function and data are
set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py add 1 2

Notes:
Function names must match the standard mathematical, logical and comparison functions from the built-in libraries.
The script must raises all happened exceptions.
For non-mathematical function need to raise NotImplementedError.
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""
import argparse
import math
import operator


parser = argparse.ArgumentParser(description='Input string to evaluate expression')
parser.add_argument('operation', metavar='operation', type=str, nargs=1,
                    help='string operation')
parser.add_argument('parameters', metavar='parameters', type=str, nargs='+',
                    help='string parameters')


def calculate(args):
    operation = args.operation[0]
    parameters = args.parameters

    res = 0
    _locals = locals()
    if operation in dir(math):
        exec(f'res = math.{operation}({",".join(parameters)})', globals(), _locals)
    elif operation in dir(operator):
        exec(f'res = operator.{operation}({",".join(parameters)})', globals(), _locals)
    else:
        raise NotImplementedError

    res = _locals['res']
    return res


def main():
    args = parser.parse_args()
    print(calculate(args))


if __name__ == '__main__':
    main()

""" Write a Python-script that determines whether the input string is the correct entry for the
'formula' according EBNF syntax (without using regular expressions).
Formula = Number | (Formula Sign Formula)
Sign = '+' | '-'
Number = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
Input: string
Result: (True / False, The result value / None)

Example,
user_input = '1+2+4-2+5-1' result = (True, 9)
user_input = '123' result = (True, 123)
user_input = 'hello+12' result = (False, None)
user_input = '2++12--3' result = (False, None)
user_input = '' result = (False, None)

Example how to call the script from CLI:
python task_1_ex_3.py 1+5-2

Hint: use argparse module for parsing arguments from CLI
"""

import argparse


parser = argparse.ArgumentParser(description='User input (formula according to EBNF) to evaluate.')
parser.add_argument('user_input', metavar='user_input', type=str,
                    help='user input formula to evaluate')


def check_formula(user_input):
    signs = [char for char in user_input if not char.isnumeric()]
    formula = user_input.replace('+', '-')
    number_parts = formula.split('-')
    edited_parts = [num_part.lstrip('0') for num_part in number_parts]

    is_formula = all([num_part.isnumeric() for num_part in number_parts])

    result = edited_parts[0]
    for num, sign in zip(edited_parts[1:], signs):
        result += f'{sign}{num}'

    return (is_formula,
            eval(result) if is_formula else None)


def main():
    args = parser.parse_args()
    print(check_formula(args.user_input))


if __name__ == '__main__':
    main()

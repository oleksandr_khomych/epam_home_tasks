"""
Implement a function `sort_names(input_file_path: str, output_file_path: str) -> None`, which sorts names from
`file_path` and write them to a new file `output_file_path`. Each name should start with a new line as in the
following example:
Example:

Adele
Adrienne
...
Willodean
Xavier
"""


def sort_names(input_file_path: str, output_file_path: str) -> None:
    with open(input_file_path, encoding='utf-8') as file:
        names = file.readlines()

    names.sort()

    with open(output_file_path, 'w', encoding='utf-8') as file:
        file.writelines(''.join(names))


if __name__ == '__main__':
    sort_names('unsorted_names.txt', 'new_file.txt')
    with open('new_file.txt', encoding='utf-8') as res_file:
        print(res_file.readlines())

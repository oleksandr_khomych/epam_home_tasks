"""Implement a function `most_common_words(file_path: str, top_words: int) -> list`
which returns most common words in the file.
<file_path> - system path to the text file
<top_words> - number of most common words to be returned

Example:

>>> print(most_common_words('lorem_ipsum.txt', 3))
['donec', 'etiam', 'aliquam']

> NOTE: Remember about dots, commas, capital letters etc.
"""
import re
from collections import Counter


def most_common_words(file_path: str, top_words: int) -> list:
    with open(file_path, encoding='utf-8') as file:
        lines = file.readlines()

    words = list()
    for line in lines:
        words.extend((word.lower() for word in re.findall(r'[a-zA-Z]+', line)))

    counter = Counter(words)
    count_words = counter.most_common(top_words)

    return list(list(zip(*count_words))[0])

"""
File `data/students.csv` stores information about students in CSV format.
This file contains the student’s names, age and average mark.

1. Implement a function get_top_performers which receives file path and
returns names of top performer students.
Example:
def get_top_performers(file_path, number_of_top_students=5):
    pass

>>> print(get_top_performers("students.csv"))
['Teresa Jones', 'Richard Snider', 'Jessica Dubose', 'Heather Garcia', 'Joseph Head']

2. Implement a function write_students_age_desc which receives the file path
with students info and writes CSV student information to the new file in
descending order of age.
Example:
def write_students_age_desc(file_path, output_file):
    pass

Content of the resulting file:
student name,age,average mark
Verdell Crawford,30,8.86
Brenda Silva,30,7.53
...
Lindsey Cummings,18,6.88
Raymond Soileau,18,7.27

>>> write_students_age_desc('students.csv', 'new_file.csv')

"""
from csv import DictReader, DictWriter


def get_top_performers(file_path: str, number_of_top_students: int = 5) -> list:
    with open(file_path) as file:
        reader = DictReader(file)
        rows = list()
        for row in reader:
            rows.append(row)

    rows.sort(key=lambda x: float(x['average mark']), reverse=True)
    return [rows[i]['student name'] for i in range(number_of_top_students)]


def write_students_age_desc(file_path: str, output_file: str):
    with open(file_path) as file:
        reader = DictReader(file)
        rows = list()
        for row in reader:
            rows.append(row)

    rows.sort(key=lambda x: int(x['age']), reverse=True)
    with open(output_file, 'w', newline='') as file:
        fieldnames = ['student name', 'age', 'average mark']
        writer = DictWriter(file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(rows)

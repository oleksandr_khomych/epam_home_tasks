"""
Task 2_3

You are given n bars of gold with weights: w1, w2, ..., wn and bag with capacity W.
There is only one instance of each bar and for each bar you can either take it or not
(hence you cannot take a fraction of a bar). Write a function that returns the maximum weight of gold that fits
into a knapsack's capacity.

The first parameter contains 'capacity' - integer describing the capacity of a knapsack
The next parameter contains 'weights' - list of weights of each gold bar
The last parameter contains 'bars_number' - integer describing the number of gold bars
Output : Maximum weight of gold that fits into a knapsack with capacity of W.

Note:
Use the argparse module to parse command line arguments. You don't need to enter names of parameters (i. e. -capacity)
Raise ValueError in case of false parameter inputs
Example of how the task should be called:
python3 task3_1.py -W 56 -w 3 4 5 6 -n 4
"""
import argparse


parser = argparse.ArgumentParser(description='Input data')
parser.add_argument('-W', metavar='capacity', type=int, help='integer describing the capacity of a knapsack')
parser.add_argument('-w', metavar='weights', type=int, nargs='+', help='list of weights of each gold bar')
parser.add_argument('-n', metavar='bars_number', type=int, help='integer describing the number of gold bars')


def bounded_knapsack(args):
    capacity = args.W
    weights = args.w
    bars_number = args.n
    print(capacity, weights, bars_number)

    if capacity <= 0 or any(weight <= 0 for weight in weights) or bars_number != len(weights) or bars_number <= 0:
        raise ValueError

    capacity_check = [0] * (capacity + 1)
    capacity_check[0] = 1

    for weight in weights:
        for k in range(capacity, weight - 1, -1):
            if capacity_check[k - weight] == 1:
                capacity_check[k] = 1

    return ''.join((str(item) for item in capacity_check)).rindex('1')


def main():
    print(bounded_knapsack(args=parser.parse_args()))


if __name__ == '__main__':
    main()

"""
Write a function converting a Roman numeral from a given string N into an Arabic numeral.
Values may range from 1 to 100 and may contain invalid symbols.
Invalid symbols and numerals out of range should raise ValueError.

Numeral / Value:
I: 1
V: 5
X: 10
L: 50
C: 100

Example:
N = 'I'; result = 1
N = 'XIV'; result = 14
N = 'LXIV'; result = 64

Example of how the task should be called:
python3 task_3_ex_2.py LXIV

Note: use `argparse` module to parse passed CLI arguments
"""
import argparse


parser = argparse.ArgumentParser(description='Parse a Roman numeral from a given string N.')
parser.add_argument('N', type=str, help='A Roman numeral string value.')


def from_roman_numerals(args):
    rom_num = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100}

    if any([char not in rom_num.keys() for char in args]):
        raise ValueError

    result = 0
    for i in range(len(args)):
        if i > 0 and rom_num[args[i]] > rom_num[args[i - 1]]:
            result += rom_num[args[i]] - 2 * rom_num[args[i - 1]]
        else:
            result += rom_num[args[i]]

    return result


def main():
    args = parser.parse_args()
    print(from_roman_numerals(args.N))


if __name__ == "__main__":
    main()

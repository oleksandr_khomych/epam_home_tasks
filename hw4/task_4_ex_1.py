"""04 Task 1.1
Implement a function which receives a string and replaces all " symbols with ' and vise versa. The
function should return modified string.
Usage of any replacing string functions is prohibited.
"""


def swap_quotes(string: str) -> str:
    return ''.join(list(map(lambda s: '"' if s == "'" else "'" if s == '"' else s, list(string))))

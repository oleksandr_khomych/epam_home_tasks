"""
Create a function sum_binary_1 that for a positive integer n
calculates the result value, which is equal to the sum of the
“1” in the binary representation of n otherwise, returns None.

Example,
n = 14 = 1110 result = 3
n = 128 = 10000000 result = 1

>>> sum_binary_1(14)
3

>>> sum_binary_1(128)
1

>>> print(sum_binary_1('123'))
None

>>> print(sum_binary_1(True))
None

"""


def sum_binary_1(n: int):
    if not (isinstance(n, int) and n > 0) or isinstance(n, bool):
        return None

    return bin(n).count('1')


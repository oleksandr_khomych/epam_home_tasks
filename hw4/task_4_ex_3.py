"""
Task04_3

Implement a function which works the same as str.split

Note:
Usage of str.split method is prohibited
Raise ValueError in case of wrong data type
"""


def split_alternative(str_to_split: str, delimiter: str = ' ') -> list:
    if not isinstance(str_to_split, str) or not isinstance(delimiter, str):
        raise ValueError

    delimiter_pos = str_to_split.find(delimiter)
    if delimiter_pos == -1:
        return [str_to_split]

    result, delimiter_prev = [], 0
    while delimiter_pos != -1:
        result.append(str_to_split[delimiter_prev: delimiter_pos])
        delimiter_prev = delimiter_pos + len(delimiter)
        delimiter_pos = str_to_split.find(delimiter, delimiter_prev)

    result.append(str_to_split[delimiter_prev:])
    return result

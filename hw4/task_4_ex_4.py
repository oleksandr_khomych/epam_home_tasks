"""
Implement a function `split_by_index(string: str, indexes: List[int]) -> List[str]`
which splits the `string` by indexes specified in `indexes`. 
Only positive index, larger than previous in list is considered valid.
Invalid indexes must be ignored.

Examples:
```python
>>> split_by_index("pythoniscool,isn'tit?", [6, 8, 12, 13, 18])
['python', 'is', 'cool', ',', "isn't", 'it?']

>>> split_by_index("pythoniscool,isn'tit?", [6, 8, 8, -4, 0, 'u', 12, 13, 18])
['python', 'is', 'cool', ',', "isn't", 'it?']

>>> split_by_index('no luck', [42])
['no luck']
```
"""


def split_by_index(string, indexes):
    edited_indexes = [0]
    for index in indexes:
        if isinstance(index, int) and edited_indexes[-1] < index < len(string):
            edited_indexes.append(index)

    edited_indexes.append(None)
    return [string[edited_indexes[i]:edited_indexes[i+1]] for i in range(len(edited_indexes)-1)]

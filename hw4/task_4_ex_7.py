"""
Task04_1_7
Implement a function foo(List[int]) -> List[int] which, given a list of integers, returns a new  or modified list
in which every element at index i of the new list is the product of all the numbers in the original array except the one at i.
Example:
`python

>>> product_array([1, 2, 3, 4, 5])
[120, 60, 40, 30, 24]

>>> product_array([3, 2, 1])
[2, 3, 6]

>>> product_array([4, 0, 9, 2])
[0, 72, 0, 0]

>>> product_array([3, 6, 0, 1, 2, 0, 4])
[0, 0, 0, 0, 0, 0, 0]

`
"""

from typing import List
from math import prod


def product_array(num_list: List[int]) -> List[int]:
    if num_list.count(0) > 1:
        return [0] * len(num_list)
    elif 0 in num_list:
        result = [0] * len(num_list)
        result[num_list.index(0)] = prod([i for i in num_list if i != 0])
        return result
    return list(map(lambda x: int(prod(num_list) / x), num_list))

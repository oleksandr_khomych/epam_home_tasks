"""
Write 2 functions:
1. Function 'is_sorted', determining whether a given list of integer values of arbitrary length
is sorted in a given order (the order is set up by enum value SortOrder).
List and sort order are passed by parameters. Function does not change the array, it returns
boolean value.

2. Function 'transform', replacing the value of each element of an integer list with the sum
of this element value and its index, only if the given list is sorted in the given order
(the order is set up by enum value SortOrder). List and sort order are passed by parameters.
To check, if the array is sorted, the function 'is_sorted' is called.

Example for 'transform' function,
For [5, 17, 24, 88, 33, 2] and “ascending” sort order values in the array do not change;
For [15, 10, 3] and “ascending” sort order values in the array do not change;
For [15, 10, 3] and “descending” sort order the values in the array are changing to [15, 11, 5]

Note:
Raise TypeError in case of wrong function arguments data type;

>>> transform([5, 17, 24, 88, 33, 2], SortOrder.ASC)
[5, 17, 24, 88, 33, 2]

>>> transform([15, 10, 3], SortOrder.ASC)
[15, 10, 3]

>>> transform([15, 10, 3], SortOrder.DESC)
[15, 11, 5]

>>> transform([1, 2, 3], 'ascending')
TypeError

>>> transform([1, 'asd', 4, 5.6], SortOrder.DESC)
TypeError

"""
from enum import Enum
from typing import List


class SortOrder(Enum):
    ASC = 'ascending'
    DESC = 'descending'


def validate_input(num_list: List[int], sort_order: SortOrder):
    if not (isinstance(num_list, list) and isinstance(sort_order, SortOrder)):
        raise TypeError
    if not all([isinstance(item, int) and not isinstance(item, bool) for item in num_list]):
        raise TypeError


def is_sorted(num_list: List[int], sort_order: SortOrder) -> bool:
    validate_input(num_list, sort_order)
    if sort_order.value == 'ascending':
        check_list = [num_list[i] <= num_list[i + 1] for i in range(len(num_list) - 1)]
    else:
        check_list = [num_list[i] >= num_list[i + 1] for i in range(len(num_list) - 1)]
    return all(check_list)


def transform(num_list: List[int], sort_order: SortOrder) -> List[int]:
    validate_input(num_list, sort_order)
    if not is_sorted(num_list, sort_order):
        return num_list
    return list(map(sum, list(enumerate(num_list))))

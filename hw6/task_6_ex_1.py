"""
Implement function combine_dicts, which receives a changeable
number of dictionaries (keys - letters, values - integers)
and combines them into one dictionary.

Dict values should be summarized in case of identical keys.

Example:
>>> dict_1 = {'a': 100, 'b': 200}
>>> dict_2 = {'a': 200, 'c': 300}
>>> dict_3 = {'a': 300, 'd': 100}

>>> combine_dicts(dict_1, dict_2)
{'a': 300, 'b': 200, 'c': 300}

>>> combine_dicts(dict_1, dict_2, dict_3)
{'a': 600, 'b': 200, 'c': 300, 'd': 100}

>>> combine_dicts({'ad': 100, 'b': 200})
KeyError

>>> combine_dicts({'a': True, 'b': [1, 2, 3]})
ValueError

"""


def combine_dicts(*args):
    new_keys = []
    for arg in args:
        if any([not (isinstance(key, str) and key.isalpha() and len(key) == 1) for key in list(arg.keys())]):
            raise KeyError
        if any([not isinstance(value, int) or isinstance(value, bool) for value in list(arg.values())]):
            raise ValueError
        new_keys.extend(list(arg.keys()))
    new_keys = set(new_keys)

    result = dict()
    for key in new_keys:
        result[key] = sum([arg[key] for arg in args if key in arg.keys()])

    return result

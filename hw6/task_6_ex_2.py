"""
Create CustomList – the linked list of values of random type, which size changes dynamically and has an ability to index
elements.

The task requires implementation of the following functionality:
• Create the empty user list and the one based on enumeration of values separated by commas. The elements are stored
in form of unidirectional linked list. Nodes of this list must be implemented in class Item.
    Method name: __init__(self, *data) -> None;
• Add and remove elements.
    Method names: append(self, value) -> None - to add to the end,
                add_start(self, value) -> None - to add to the start,
                remove(self, value) -> None - to remove the first occurrence of given value;
• Operations with elements by index. Negative indexing is not necessary.
    Method names: __getitem__(self, index) -> Any,
                __setitem__(self, index, data) -> None,
                __delitem__(self, index) -> None;
• Receive index of predetermined value.
    Method name: find(self, value) -> Any;
• Clear the list.
    Method name: clear(self) -> None;
• Receive lists length.
    Method name: __len__(self) -> int;
• Make CustomList iterable to use in for-loops;
    Method name: __iter__(self);
• Raise exceptions when:
    find() or remove() don't find specific value
    index out of bound at methods __getitem__, __setitem__, __delitem__.


Notes:
    The class CustomList must not inherit from anything (except from the object, of course).
    Function names should be as described above. Additional functionality has no naming requirements.
    Indexation starts with 0.
    List length changes while adding and removing elements.
    Inside the list the elements are connected as in a linked list, starting with link to head.
"""


class Item:
    def __init__(self, data=None):
        self.node = data
        self.next = None


class CustomList:
    def __init__(self, *data):
        if not data:
            self.head = None
        else:
            prev = Item(data[0])
            self.head = prev
            for item in data[1:]:
                current = Item(item)
                prev.next = current
                prev = current

    def append(self, value):
        new_node = Item(value)
        if self.head is None:
            self.head = new_node
        else:
            last = self.head
            while last.next:
                last = last.next
            last.next = new_node

    def add_start(self, value):
        new_node = Item(value)
        new_node.next = self.head
        self.head = new_node

    def remove(self, value):
        head = self.head

        if head is None:
            raise ValueError

        if head is not None and head.node == value:
            self.head = head.next
            head = None

        else:
            while head is not None and head.node != value:
                prev = head
                head = head.next

            if not head:
                raise ValueError

            prev.next = head.next
            head = None

    def __getitem__(self, index):
        if self.head is None:
            raise IndexError

        head = self.head
        ind = 0
        while ind < index and head is not None:
            head = head.next
            ind += 1

        if head:
            return head.node
        else:
            raise IndexError

    def __setitem__(self, index, data):
        if self.head is None:
            raise IndexError

        head = self.head
        ind = 0
        while ind < index and head is not None:
            head = head.next
            ind += 1

        if head:
            head.node = data
        else:
            raise IndexError

    def __delitem__(self, index):
        if self.head is None:
            raise IndexError

        head = self.head
        if index == 0:
            self.head = head.next
            head = None
        else:
            ind = 0
            while ind < index and head is not None:
                prev = head
                head = head.next
                ind += 1

            if head:
                prev.next = head.next
                head = None
            else:
                raise IndexError

    def find(self, value):
        if self.head is None:
            raise ValueError

        head = self.head
        ind = 0
        while head is not None and head.node != value:
            head = head.next
            ind += 1

        if head:
            return ind
        else:
            raise ValueError

    def clear(self):
        self.head = None

    def __len__(self):
        if not self.head:
            return 0
        length = 1
        head = self.head
        while head.next:
            head = head.next
            length += 1

        return length

    def __iter__(self):
        self.current = self.head
        return self

    def __next__(self):
        if self.current:
            result = self.current.node
            self.current = self.current.next
            return result
        else:
            raise StopIteration
#
#
# def main():
#     my_list = CustomList(1, 6, 'asd', [123, 5467])
#     print(len(my_list))
#     my_list.append('hello')
#     my_list.add_start('end')
#     print(len(my_list))
#     del my_list[3]
#     print(len(my_list), end='\n\n')
#
#     for item in my_list:
#         print(item)
#
#
# if __name__=='__main__':
#     main()

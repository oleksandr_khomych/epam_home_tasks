"""
Write a function that checks whether a string is a palindrome or not.
Return 'True' if it is a palindrome, else 'False'.

Note:
Usage of reversing functions is required.
Raise ValueError in case of wrong data type

To check your implementation you can use strings from here
(https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes).

>>> is_palindrome('Able was I ere I saw Elba')
True

>>> is_palindrome('qwertyqwertyqwerty')
False

>>> is_palindrome('A man, a plan, a canal – Panama')
True

"""


def is_palindrome(test_string: str) -> bool:
    if not isinstance(test_string, str):
        raise ValueError
    test_string = ''.join([i.lower() for i in test_string if i.isalpha()])
    return test_string == ''.join(reversed(test_string))


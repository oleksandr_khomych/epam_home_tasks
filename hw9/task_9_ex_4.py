"""
Implement a bunch of functions which receive a changeable number of strings and return next
parameters:
1) characters that appear in all strings
2) characters that appear in at least one string
3) characters that appear at least in two strings
  Note: raise ValueError if there are less than two strings
4) characters of alphabet, that were not used in any string
  Note: use `string.ascii_lowercase` for list of alphabet letters

Note: raise TypeError in case of wrong data type

Examples,
```python
>>> test_strings = ["hello", "world", "python", ]
>>> print(chars_in_all(*test_strings))
{'o'}

>>> print(chars_in_one(*test_strings))
{'d', 'e', 'h', 'l', 'n', 'o', 'p', 'r', 't', 'w', 'y'}

>>> print(chars_in_two(*test_strings))
{'h', 'l', 'o'}

>>> print(not_used_chars(*test_strings))
{'q', 'k', 'g', 'f', 'j', 'u', 'a', 'c', 'x', 'm', 'v', 's', 'b', 'z', 'i'}

"""
import string


def chars_in_all(*strings):
    result = set(string.ascii_lowercase)
    for s in strings:
        if not isinstance(s, str):
            raise TypeError
        result &= set(s)

    return result


def chars_in_one(*strings):
    result = set()
    for s in strings:
        if not isinstance(s, str):
            raise TypeError
        result |= set(s)

    return result


def chars_in_two(*strings):
    if len(strings) < 2:
        raise ValueError

    list_words = list()
    for s in strings:
        if not isinstance(s, str):
            raise TypeError
        list_words.extend(list(set(s)))

    return set(char for char in list_words if list_words.count(char) >= 2)


def not_used_chars(*strings):
    result = set()
    for s in strings:
        if not isinstance(s, str):
            raise TypeError
        result |= set(s)

    return set(string.ascii_lowercase) - result
